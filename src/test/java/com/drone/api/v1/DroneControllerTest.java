package com.drone.api.v1;

import static org.mockito.ArgumentMatchers.any;

import com.drone.domain.BatteryCapacity;
import com.drone.domain.Drone;
import com.drone.domain.DroneId;
import com.drone.domain.DroneModel;
import com.drone.domain.Serial;
import com.drone.domain.State;
import com.drone.domain.Weight;
import com.drone.domain.exception.DroneNotFoundException;
import com.drone.domain.service.DroneDomainService;
import java.math.BigInteger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class DroneControllerTest {

  @Mock private DroneDomainService droneDomainService;

  @InjectMocks private DroneController droneController;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(droneDomainService);
  }

  @Test
  public void shouldThrowDroneNotFoundExceptionWhenDroneByIdNotFound() {
    BDDMockito.given(this.droneDomainService.retrieveById(any())).willReturn(Mono.empty());

    var publisher = this.droneController.getDroneById("asdfasdf");

    StepVerifier.create(publisher).expectError(DroneNotFoundException.class).verify();
  }

  @Test
  public void shouldReturn200WhenDroneByIdFound() {
    var id1 = DroneId.of("id1");
    BDDMockito.given(this.droneDomainService.retrieveById(id1))
        .willReturn(Mono.just(this.getDroneMock()));

    var publisher = this.droneController.getDroneById("id1");

    StepVerifier.create(publisher)
        .assertNext(
            responseEntity -> {
              Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
              Assertions.assertEquals("id1", responseEntity.getBody().getId());
              Assertions.assertEquals(
                  BigInteger.TEN.toString(), responseEntity.getBody().getSerialNumber());
              Assertions.assertEquals(
                  DroneModel.Cruiserweight.name(), responseEntity.getBody().getModel());
              Assertions.assertEquals(12.3, responseEntity.getBody().getWeight());
              Assertions.assertEquals(12, responseEntity.getBody().getBatteryCapacity());
              Assertions.assertEquals(State.IDLE.name(), responseEntity.getBody().getState());
            })
        .verifyComplete();
  }

  private Drone getDroneMock() {
    return Drone.builder()
        .droneId(DroneId.of("id1"))
        .serial(Serial.of(BigInteger.TEN))
        .droneModel(DroneModel.Cruiserweight)
        .weight(Weight.of(12.3))
        .batteryCapacity(BatteryCapacity.of(12))
        .state(State.IDLE)
        .build();
  }
}
