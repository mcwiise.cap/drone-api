package com.drone.domain.service;

import static org.mockito.ArgumentMatchers.any;

import com.drone.domain.DroneId;
import com.drone.domain.DroneModel;
import com.drone.domain.State;
import com.drone.jpa.repository.DroneJpaRepository;
import com.drone.jpa.repository.entity.DroneEntity;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class DroneDomainServiceImplTest {

  @Mock private DroneJpaRepository droneJpaRepository;

  @InjectMocks private DroneDomainServiceImpl droneDomainService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(droneJpaRepository);
  }

  @Test
  public void shouldEmptyMonoWhenDroneByIdNotFound() {
    BDDMockito.given(this.droneJpaRepository.findById(any())).willReturn(Optional.empty());
    var publisher = this.droneDomainService.retrieveById(DroneId.of("adfadf"));
    StepVerifier.create(publisher).expectNextCount(0).verifyComplete();
  }

  @Test
  public void shouldReturn1EventWhenDroneByIdWasFound() {
    var droneId = DroneId.of("id1");
    var entityFound = this.getDroneEntityMock();
    BDDMockito.given(this.droneJpaRepository.findById(any())).willReturn(Optional.of(entityFound));

    var publisher = this.droneDomainService.retrieveById(droneId);

    StepVerifier.create(publisher)
        .assertNext(
            response -> {
              Assertions.assertEquals(entityFound.getId(), response.getDroneId().getValue());
              Assertions.assertEquals(
                  entityFound.getSerial(), response.getSerial().getValue().toString());
              Assertions.assertEquals(entityFound.getDroneModel(), response.getDroneModel().name());
              Assertions.assertEquals(entityFound.getWeight(), response.getWeight().getValue());
              Assertions.assertEquals(
                  entityFound.getBatteryCapacity(), response.getBatteryCapacity().getValue());
              Assertions.assertEquals(entityFound.getState(), response.getState().name());
            })
        .verifyComplete();
  }

  private DroneEntity getDroneEntityMock() {
    var entity = new DroneEntity();
    entity.setId("id1");
    entity.setSerial("12341234345823749582734985");
    entity.setDroneModel(DroneModel.Cruiserweight.name());
    entity.setWeight(123.12);
    entity.setBatteryCapacity(12);
    entity.setState(State.IDLE.name());
    return entity;
  }
}
