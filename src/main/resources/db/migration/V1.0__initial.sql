CREATE TABLE public.drone (
	id VARCHAR ( 50 ) PRIMARY KEY NOT NULL,
	serial VARCHAR ( 100 )  UNIQUE NOT NULL,
    drone_model VARCHAR ( 30 ) NOT NULL,
	weight_limit DECIMAL NOT NULL,
	battery_capacity INTEGER NOT NULL,
	state VARCHAR ( 30 ) NOT NULL,
	current_weight DECIMAL DEFAULT 0.0
);

CREATE TABLE public.medication (
	id VARCHAR ( 50 ) PRIMARY KEY NOT NULL,
	name VARCHAR ( 50 ) NOT NULL,
	weight DECIMAL NOT NULL,
	code VARCHAR ( 50 ) NOT NULL,
	image VARCHAR ( 1000 ) NOT null
);

CREATE TABLE public.drone_medication (
	drone_id VARCHAR ( 50 ) not null references public.drone(id) ON UPDATE CASCADE ON DELETE CASCADE,
	medication_id VARCHAR ( 50 ) not null references public.medication(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO public.drone
(id, serial, drone_model, weight_limit, battery_capacity, state)
VALUES('id1', '23419238409384592345', 'Cruiserweight', 12.667, 13, 'IDLE');
INSERT INTO public.drone
(id, serial, drone_model, weight_limit, battery_capacity, state)
VALUES('id2', '123153', 'Cruiserweight', 20.0, 26, 'IDLE');
