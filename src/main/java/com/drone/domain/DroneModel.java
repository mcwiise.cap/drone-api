package com.drone.domain;

public enum DroneModel {
  Lightweight,
  Middleweight,
  Cruiserweight,
  Heavyweight;
}
