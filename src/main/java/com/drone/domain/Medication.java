package com.drone.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Builder(toBuilder = true)
@Getter
@Setter
@Jacksonized
public class Medication {
  private MedicationId medicationId;
  private Name name;
  private Weight weight;
  private Code code;
  private ImageBase64 imageBase64;
}
