package com.drone.domain.service;

import com.drone.domain.Drone;
import com.drone.domain.DroneId;
import com.drone.domain.Medication;
import com.drone.domain.State;
import java.util.List;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DroneDomainService {
  Mono<Drone> retrieveById(DroneId droneId);

  Mono<Drone> createDrone(Drone drone);

  Mono<Drone> loadMedications(DroneId droneId, List<Medication> medications);

  Flux<Drone> retrieveByState(State state);
}
