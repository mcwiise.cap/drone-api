package com.drone.domain.service;

import com.drone.domain.BatteryCapacity;
import com.drone.domain.Code;
import com.drone.domain.CurrentWeight;
import com.drone.domain.Drone;
import com.drone.domain.DroneId;
import com.drone.domain.DroneModel;
import com.drone.domain.ImageBase64;
import com.drone.domain.Medication;
import com.drone.domain.MedicationId;
import com.drone.domain.Name;
import com.drone.domain.Serial;
import com.drone.domain.State;
import com.drone.domain.Weight;
import com.drone.domain.WeightLimit;
import com.drone.domain.exception.DroneAlreadyExistsException;
import com.drone.domain.exception.DroneInsufficientBatteryCapacityException;
import com.drone.domain.exception.MedicationsOvercomeDroneWeightLimitException;
import com.drone.jpa.repository.DroneJpaRepository;
import com.drone.jpa.repository.MedicationJpaRepository;
import com.drone.jpa.repository.entity.DroneEntity;
import com.drone.jpa.repository.entity.MedicationEntity;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class DroneDomainServiceImpl implements DroneDomainService {

  private static final Integer MIN_BATTERY_LEVEL = 25;
  private Logger logger = LoggerFactory.getLogger(DroneDomainServiceImpl.class);
  private final DroneJpaRepository droneJpaRepository;
  private final MedicationJpaRepository medicationJpaRepository;

  public DroneDomainServiceImpl(
      DroneJpaRepository droneJpaRepository, MedicationJpaRepository medicationJpaRepository) {
    this.medicationJpaRepository = medicationJpaRepository;
    this.droneJpaRepository = droneJpaRepository;
  }

  @Override
  public Mono<Drone> retrieveById(DroneId droneId) {
    return Mono.justOrEmpty(droneJpaRepository.findById(droneId.getValue()))
        .map(DroneDomainServiceImpl::fromJpaEntityToDomainEntity);
  }

  @Override
  public Mono<Drone> createDrone(Drone drone) {
    if (droneJpaRepository.existsBySerial(drone.getSerial().getValue().toString())) {
      return Mono.error(
          new DroneAlreadyExistsException(
              "Drone with serialNumber: "
                  + drone.getSerial().getValue()
                  + " already exists in the database"));
    } else {
      var savedEntity = droneJpaRepository.save(fromDomainEntityToJpaEntity(drone));
      return Mono.just(fromJpaEntityToDomainEntity(savedEntity));
    }
  }

  @Override
  public Mono<Drone> loadMedications(DroneId droneId, List<Medication> medications) {
    return this.droneJpaRepository
        .findById(droneId.getValue())
        .map(
            droneEntityFound -> {
              var medicationEntities =
                  medications.stream()
                      .map(DroneDomainServiceImpl::fromDomainEntityToJpaEntity)
                      .collect(Collectors.toSet());
              var medTotalWeight =
                  medicationEntities.stream().mapToDouble(MedicationEntity::getWeight).sum();
              if (droneEntityFound.getBatteryCapacity() < MIN_BATTERY_LEVEL) {
                logger.warn("Drone battery capacity is under 25%. droneId: {}", droneId.getValue());
                return Mono.error(
                        new DroneInsufficientBatteryCapacityException(
                            "Drone battery capacity is under 25% "))
                    .cast(Drone.class);
              } else if (medTotalWeight > droneEntityFound.getWeightLimit()) {
                logger.warn(
                    "Medications to load overcomes drone weight limit. droneId: {}, drone weight limit {}, medications weight {}",
                    droneId.getValue(),
                    droneEntityFound.getWeightLimit(),
                    medTotalWeight);
                return Mono.error(
                        new MedicationsOvercomeDroneWeightLimitException(
                            "Medications to load overcomes drone weight limit. drone weight limit: "
                                + droneEntityFound.getWeightLimit()
                                + ", medications weight: "
                                + medTotalWeight))
                    .cast(Drone.class);
              }
              var nWeight = droneEntityFound.getCurrentWeight() + medTotalWeight;
              if (nWeight > droneEntityFound.getWeightLimit()) {
                logger.warn(
                    "Current drone weight overcomes weight limit. droneId: {}, drone weight limit {}, current weight {}",
                    droneId.getValue(),
                    droneEntityFound.getWeightLimit(),
                    nWeight);
                return Mono.error(
                        new MedicationsOvercomeDroneWeightLimitException(
                            "Current drone weight overcomes weight limit. drone weight limit: "
                                + droneEntityFound.getWeightLimit()
                                + ", current weight: "
                                + nWeight))
                    .cast(Drone.class);
              } else {
                var savedMedications =
                    StreamSupport.stream(
                            this.medicationJpaRepository.saveAll(medicationEntities).spliterator(),
                            false)
                        .collect(Collectors.toSet());
                droneEntityFound.setMedications(savedMedications);
                droneEntityFound.setCurrentWeight(nWeight);
                if (droneEntityFound.getCurrentWeight() >= droneEntityFound.getWeightLimit()) {
                  droneEntityFound.setState(State.LOADED.name());
                } else {
                  droneEntityFound.setState(State.LOADING.name());
                }
                return Mono.just(this.droneJpaRepository.save(droneEntityFound))
                    .map(DroneDomainServiceImpl::fromJpaEntityToDomainEntity);
              }
            })
        .orElseGet(Mono::empty);
  }

  @Override
  public Flux<Drone> retrieveByState(State state) {
    return Flux.fromIterable(this.droneJpaRepository.findByState(state.name()))
        .map(DroneDomainServiceImpl::fromJpaEntityToDomainEntity);
  }

  private static Drone fromJpaEntityToDomainEntity(DroneEntity entity) {
    return Drone.builder()
        .droneId(DroneId.of(entity.getId()))
        .serial(Serial.of(new BigInteger(entity.getSerial())))
        .droneModel(DroneModel.valueOf(entity.getDroneModel()))
        .weightLimit(WeightLimit.of(entity.getWeightLimit()))
        .batteryCapacity(BatteryCapacity.of(entity.getBatteryCapacity()))
        .state(State.valueOf(entity.getState()))
        .currentWeight(CurrentWeight.of(entity.getCurrentWeight()))
        .medications(
            entity.getMedications().stream()
                .map(DroneDomainServiceImpl::fromJpaEntityToDomainEntity)
                .toList())
        .build();
  }

  private static Medication fromJpaEntityToDomainEntity(MedicationEntity entity) {
    return Medication.builder()
        .medicationId(MedicationId.of(entity.getId()))
        .name(Name.of(entity.getName()))
        .weight(Weight.of(entity.getWeight()))
        .code(Code.of(entity.getCode()))
        .imageBase64(ImageBase64.of(entity.getImage()))
        .build();
  }

  private static DroneEntity fromDomainEntityToJpaEntity(Drone drone) {
    var entity = new DroneEntity();
    entity.setId(drone.getDroneId().getValue());
    entity.setSerial(drone.getSerial().getValue().toString());
    entity.setDroneModel(drone.getDroneModel().name());
    entity.setWeightLimit(drone.getWeightLimit().getValue());
    entity.setBatteryCapacity(drone.getBatteryCapacity().getValue());
    entity.setState(drone.getState().name());
    entity.setCurrentWeight(drone.getCurrentWeight().getValue());
    entity.setMedications(
        drone.getMedications().stream()
            .map(DroneDomainServiceImpl::fromDomainEntityToJpaEntity)
            .collect(Collectors.toSet()));
    return entity;
  }

  private static MedicationEntity fromDomainEntityToJpaEntity(Medication medication) {
    var entity = new MedicationEntity();
    entity.setId(medication.getMedicationId().getValue());
    entity.setName(medication.getName().getValue());
    entity.setWeight(medication.getWeight().getValue());
    entity.setCode(medication.getCode().getValue());
    entity.setImage(medication.getImageBase64().getValue());
    return entity;
  }
}
