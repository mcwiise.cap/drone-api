package com.drone.domain;

public enum State {
  IDLE,
  LOADING,
  LOADED,
  DELIVERING,
  DELIVERED,
  RETURNING;
}
