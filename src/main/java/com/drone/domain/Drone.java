package com.drone.domain;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Builder(toBuilder = true)
@Getter
@Setter
@Jacksonized
public class Drone {
  private DroneId droneId;
  private Serial serial;
  private DroneModel droneModel;
  private WeightLimit weightLimit;
  private BatteryCapacity batteryCapacity;
  private State state;
  private CurrentWeight currentWeight;
  private List<Medication> medications;
}
