package com.drone.domain.exception;

public class DroneAlreadyExistsException extends RuntimeException {
  public DroneAlreadyExistsException() {
    super();
  }

  public DroneAlreadyExistsException(String message) {
    super(message);
  }
}
