package com.drone.domain.exception;

public class DroneInsufficientBatteryCapacityException extends RuntimeException {
  public DroneInsufficientBatteryCapacityException() {
    super();
  }

  public DroneInsufficientBatteryCapacityException(String message) {
    super(message);
  }
}
