package com.drone.domain.exception;

public class DroneNotFoundException extends RuntimeException {

  public DroneNotFoundException() {
    super();
  }

  public DroneNotFoundException(String message) {
    super(message);
  }
}
