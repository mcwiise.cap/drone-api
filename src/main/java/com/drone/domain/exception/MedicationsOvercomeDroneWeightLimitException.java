package com.drone.domain.exception;

public class MedicationsOvercomeDroneWeightLimitException extends RuntimeException {
  public MedicationsOvercomeDroneWeightLimitException() {
    super();
  }

  public MedicationsOvercomeDroneWeightLimitException(String message) {
    super(message);
  }
}
