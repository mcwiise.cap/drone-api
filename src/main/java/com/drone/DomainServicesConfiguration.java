package com.drone;

import com.drone.domain.service.DroneDomainService;
import com.drone.domain.service.DroneDomainServiceImpl;
import com.drone.jpa.repository.DroneJpaRepository;
import com.drone.jpa.repository.MedicationJpaRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainServicesConfiguration {

  @Bean
  public DroneDomainService getDroneDomainServiceBean(
      DroneJpaRepository droneJpaRepository, MedicationJpaRepository medicationJpaRepository) {
    return new DroneDomainServiceImpl(droneJpaRepository, medicationJpaRepository);
  }
}
