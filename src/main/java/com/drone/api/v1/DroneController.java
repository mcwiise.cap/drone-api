package com.drone.api.v1;

import com.drone.api.v1.dto.request.DroneRequest;
import com.drone.api.v1.dto.request.MedicationRequest;
import com.drone.api.v1.dto.response.DroneResponse;
import com.drone.api.v1.dto.response.MedicationResponse;
import com.drone.domain.BatteryCapacity;
import com.drone.domain.Code;
import com.drone.domain.CurrentWeight;
import com.drone.domain.Drone;
import com.drone.domain.DroneId;
import com.drone.domain.ImageBase64;
import com.drone.domain.Medication;
import com.drone.domain.MedicationId;
import com.drone.domain.Name;
import com.drone.domain.Serial;
import com.drone.domain.State;
import com.drone.domain.Weight;
import com.drone.domain.WeightLimit;
import com.drone.domain.exception.DroneNotFoundException;
import com.drone.domain.service.DroneDomainService;
import jakarta.validation.Valid;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/drone-api/v1")
public class DroneController {

  private final DroneDomainService droneDomainService;

  public DroneController(DroneDomainService droneDomainService) {
    this.droneDomainService = droneDomainService;
  }

  @GetMapping(value = "/drones/{droneId}")
  public Mono<ResponseEntity<DroneResponse>> getDroneById(
      @PathVariable(value = "droneId") String droneId) {
    return this.droneDomainService
        .retrieveById(DroneId.of(droneId))
        .switchIfEmpty(
            Mono.error(new DroneNotFoundException("Drone was not found, droneId: " + droneId)))
        .map(DroneController::fromDomainEntityToDtoResponse)
        .map(ResponseEntity::ok);
  }

  @GetMapping(value = "/drones")
  public ResponseEntity<Flux<DroneResponse>> getDroneByState(
      @RequestParam(value = "state") State state) {
    return ResponseEntity.ok(
        this.droneDomainService
            .retrieveByState(state)
            .map(DroneController::fromDomainEntityToDtoResponse));
  }

  @PostMapping(value = "/drones")
  public Mono<ResponseEntity<DroneResponse>> postDrone(
      @Valid @RequestBody DroneRequest requestBody) {
    return this.droneDomainService
        .createDrone(fromRequestBodyToDomainEntity(requestBody))
        .map(DroneController::fromDomainEntityToDtoResponse)
        .map(droneResponse -> new ResponseEntity<>(droneResponse, HttpStatus.CREATED));
  }

  @PutMapping(value = "/drones/{droneId}")
  public Mono<ResponseEntity<DroneResponse>> putDrone(
      @PathVariable String droneId, @RequestBody List<MedicationRequest> requestBody) {
    var medications = requestBody.stream().map(this::fromRequestBodyToDomainEntity).toList();
    return this.droneDomainService
        .loadMedications(DroneId.of(droneId), medications)
        .switchIfEmpty(
            Mono.error(new DroneNotFoundException("Drone was not found, droneId: " + droneId)))
        .map(DroneController::fromDomainEntityToDtoResponse)
        .map(ResponseEntity::ok);
  }

  private Drone fromRequestBodyToDomainEntity(DroneRequest requestBody) {
    return Drone.builder()
        .droneId(DroneId.of(UUID.randomUUID().toString()))
        .serial(Serial.of(new BigInteger(requestBody.getSerialNumber())))
        .droneModel(requestBody.getModel())
        .weightLimit(WeightLimit.of(requestBody.getWeightLimit()))
        .batteryCapacity(BatteryCapacity.of(requestBody.getBatteryCapacity()))
        .state(State.IDLE)
        .currentWeight(CurrentWeight.of(0.0))
        .medications(Collections.emptyList())
        .build();
  }

  private Medication fromRequestBodyToDomainEntity(MedicationRequest requestBody) {
    return Medication.builder()
        .medicationId(MedicationId.of(UUID.randomUUID().toString()))
        .name(Name.of(requestBody.getName()))
        .weight(Weight.of(requestBody.getWeight()))
        .code(Code.of(requestBody.getCode()))
        .imageBase64(ImageBase64.of(requestBody.getImage()))
        .build();
  }

  private static DroneResponse fromDomainEntityToDtoResponse(Drone drone) {
    var response = new DroneResponse();
    response.setId(drone.getDroneId().getValue());
    response.setSerialNumber(drone.getSerial().getValue().toString());
    response.setModel(drone.getDroneModel().name());
    response.setWeightLimit(drone.getWeightLimit().getValue());
    response.setBatteryCapacity(drone.getBatteryCapacity().getValue());
    response.setState(drone.getState().name());
    response.setCurrentWeight(drone.getCurrentWeight().getValue());
    response.setMedications(
        drone.getMedications().stream()
            .map(DroneController::fromDomainEntityToDtoResponse)
            .toList());
    return response;
  }

  private static MedicationResponse fromDomainEntityToDtoResponse(Medication medication) {
    var response = new MedicationResponse();
    response.setId(medication.getMedicationId().getValue());
    response.setName(medication.getName().getValue());
    response.setWeight(medication.getWeight().getValue());
    response.setCode(medication.getCode().getValue());
    response.setImage(medication.getImageBase64().getValue());
    return response;
  }
}
