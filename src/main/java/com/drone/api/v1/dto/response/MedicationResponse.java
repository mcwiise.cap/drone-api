package com.drone.api.v1.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicationResponse {
  private String id;
  private String name;
  private Double weight;
  private String code;
  private String image;
}
