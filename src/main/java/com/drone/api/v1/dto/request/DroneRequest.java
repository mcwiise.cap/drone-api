package com.drone.api.v1.dto.request;

import com.drone.domain.DroneModel;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DroneRequest {
  @NotNull(message = "is mandatory")
  @Pattern(regexp = "^\\d+$", message = "just numbers are allowed")
  @Size(max = 100, message = "no more than 100 digits are allowed")
  private String serialNumber;

  @NotNull(message = "is mandatory")
  private DroneModel model;

  @NotNull(message = "is mandatory")
  private Double weightLimit;

  @NotNull(message = "is mandatory")
  private Integer batteryCapacity;
}
