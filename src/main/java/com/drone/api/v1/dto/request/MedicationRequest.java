package com.drone.api.v1.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicationRequest {

  @NotNull private String name;

  @NotNull private Double weight;

  @NotNull private String code;

  @NotNull private String image;
}
