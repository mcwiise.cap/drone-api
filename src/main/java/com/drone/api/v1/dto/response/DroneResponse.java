package com.drone.api.v1.dto.response;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DroneResponse {
  private String id;
  private String serialNumber;
  private String model;
  private Double weightLimit;
  private Integer batteryCapacity;
  private String state;
  private Double currentWeight;
  private List<MedicationResponse> medications;
}
