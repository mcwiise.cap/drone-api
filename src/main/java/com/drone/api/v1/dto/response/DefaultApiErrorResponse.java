package com.drone.api.v1.dto.response;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DefaultApiErrorResponse {
  private LocalDateTime timestamp;
  private String message;
}
