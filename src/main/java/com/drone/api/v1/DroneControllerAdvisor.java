package com.drone.api.v1;

import com.drone.api.v1.dto.response.DefaultApiErrorResponse;
import com.drone.domain.exception.DroneAlreadyExistsException;
import com.drone.domain.exception.DroneInsufficientBatteryCapacityException;
import com.drone.domain.exception.DroneNotFoundException;
import com.drone.domain.exception.MedicationsOvercomeDroneWeightLimitException;
import java.time.LocalDateTime;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;

@ControllerAdvice
public class DroneControllerAdvisor {

  private Logger logger = LoggerFactory.getLogger(DroneControllerAdvisor.class);

  @ExceptionHandler(DroneNotFoundException.class)
  public final Mono<ResponseEntity<DefaultApiErrorResponse>> handleDroneNotFoundExceptionException(
      DroneNotFoundException e) {
    logger.warn(e.getMessage());
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(LocalDateTime.now(), "The drone was not found");
    return Mono.just(new ResponseEntity<>(response, HttpStatus.NOT_FOUND));
  }

  @ExceptionHandler(WebExchangeBindException.class)
  public Mono<ResponseEntity<DefaultApiErrorResponse>> handleWebExchangeBindException(
      WebExchangeBindException ex) {
    logger.error("It is not possible to attend the request. ", ex);
    var errorFields = getDetailedMessageFromArguments(ex.getDetailMessageArguments());
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(
            LocalDateTime.now(), "Mandatory fields are malformed: " + errorFields);
    return Mono.just(new ResponseEntity<>(response, HttpStatus.BAD_REQUEST));
  }

  private static String getDetailedMessageFromArguments(Object[] arguments) {
    return Arrays.stream(arguments).reduce("", (arg1, arg2) -> arg1 + " " + arg2).toString();
  }

  @ExceptionHandler(ServerWebInputException.class)
  public Mono<ResponseEntity<DefaultApiErrorResponse>> handleServerWebInputException(
      ServerWebInputException ex) {
    logger.error("It is not possible to attend the request. ", ex);
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(
            LocalDateTime.now(), "Unexpected request body: " + ex.getCause());
    return Mono.just(new ResponseEntity<>(response, HttpStatus.BAD_REQUEST));
  }

  @ExceptionHandler(DroneAlreadyExistsException.class)
  public Mono<ResponseEntity<DefaultApiErrorResponse>> handleDroneAlreadyExistsException(
      DroneAlreadyExistsException ex) {
    logger.error("It is not possible to attend the request. ", ex);
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(LocalDateTime.now(), ex.getMessage());
    return Mono.just(new ResponseEntity<>(response, HttpStatus.CONFLICT));
  }

  @ExceptionHandler(DroneInsufficientBatteryCapacityException.class)
  public Mono<ResponseEntity<DefaultApiErrorResponse>>
      handleDroneInsufficientBatteryCapacityException(
          DroneInsufficientBatteryCapacityException ex) {
    logger.error("It is not possible to attend the request. ", ex);
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(LocalDateTime.now(), ex.getMessage());
    return Mono.just(new ResponseEntity<>(response, HttpStatus.CONFLICT));
  }

  @ExceptionHandler(MedicationsOvercomeDroneWeightLimitException.class)
  public Mono<ResponseEntity<DefaultApiErrorResponse>>
      handleMedicationsOvercomeDroneWeightLimitException(
          MedicationsOvercomeDroneWeightLimitException ex) {
    logger.error("It is not possible to attend the request. ", ex);
    DefaultApiErrorResponse response =
        new DefaultApiErrorResponse(LocalDateTime.now(), ex.getMessage());
    return Mono.just(new ResponseEntity<>(response, HttpStatus.CONFLICT));
  }
}
