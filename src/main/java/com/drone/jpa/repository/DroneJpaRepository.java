package com.drone.jpa.repository;

import com.drone.jpa.repository.entity.DroneEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneJpaRepository extends CrudRepository<DroneEntity, String> {
  boolean existsBySerial(String value);

  List<DroneEntity> findByState(String name);
}
