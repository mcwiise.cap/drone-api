package com.drone.jpa.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "medication")
public class MedicationEntity {
  @Id private String id;
  private String name;
  private Double weight;
  private String code;
  private String image;

  @ManyToMany(mappedBy = "medications", fetch = FetchType.EAGER)
  Set<DroneEntity> drones;
}
