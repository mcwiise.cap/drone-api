package com.drone.jpa.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "drone")
public class DroneEntity {
  @Id private String id;
  private String serial;
  private String droneModel;
  private Double weightLimit;
  private Integer batteryCapacity;
  private String state;
  private Double currentWeight;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "drone_medication",
      joinColumns = @JoinColumn(name = "drone_id"),
      inverseJoinColumns = @JoinColumn(name = "medication_id"))
  private Set<MedicationEntity> medications;
}
