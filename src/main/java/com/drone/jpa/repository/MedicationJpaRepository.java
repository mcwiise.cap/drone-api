package com.drone.jpa.repository;

import com.drone.jpa.repository.entity.MedicationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationJpaRepository extends CrudRepository<MedicationEntity, String> {}
