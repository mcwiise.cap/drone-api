# Drone Simulator API

This API implements the following functionality:

1. Register a drone
2. Loading a drone with a set of medications
3. Retrieve loaded medications for a given drone
4. Check available drones for loading
5. Check drone battery level.

## Getting Started

1. Please make sure to install the following tools before running the project:
   * Docker with docker compose. [download from here](https://www.docker.com/)
   * Java 17. [download from here](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
   * Git. [download from here](https://git-scm.com/downloads)
2. Clone the following repository in a local folder
    ```bash 
    git clone git@gitlab.com:mcwiise.cap/drone-api.git
    ```

## How to Run...

Drone api is a spting boot application wrapped as a gradle module which uses a relational database in postgres 
to store the state of a drone along with medications.

### As Spring Boot App

From the root folder, please execute the following commands:

1. Start postgres docker container: `docker-compose up -d --build`
2. Check the postgress docker container is running ok: `docker ps -a`
3. Start the api: `./gradlew bootRun`

Check the following log in the console, to make sure the app has started on port 8080:

![Screenshot 2023-03-06 at 9.42.04 PM.png](..%2F..%2F..%2F..%2Fvar%2Ffolders%2F0z%2F_16vrq513jd_2dx79cwrm4yh0000gn%2FT%2FTemporaryItems%2FNSIRD_screencaptureui_zwHNSr%2FScreenshot%202023-03-06%20at%209.42.04%20PM.png)

### Unit tests

Unit tests for the project has been written with Junit and Mockito BDD, we can execute the suit by:

```bash
./gradlew clean build test
```

## Playing with the API

From any http client like postman or curl, we can perform the following actions

1. Register a drone:

   ```bash
   curl --location --request POST 'http://localhost:8080/drone-api/v1/drones' \
   --header 'Content-Type: application/json' \
   --data-raw '{
       "serialNumber": "1234567899999",
       "model": "Cruiserweight",
       "weightLimit": 10000.0,
       "batteryCapacity": 50
   }'
   ```
   
2. Loading a drone with a set of medications

   ```bash 
   curl --location --request PUT 'http://localhost:8080/drone-api/v1/drones/{droneId}' \
   --header 'Content-Type: application/json' \
   --data-raw '[
       {
           "name": "medication name",
           "weight": 1,
           "code": "ADD",
           "image": "adfasdfasdfasdf"
       }
   ]'
   ```

3. Retrieve loaded medications for a given drone

   ```bash 
   curl --location --request GET 'http://localhost:8080/drone-api/v1/drones/{droneId}'
   ```

4. Check available drones for loading. 

   ```bash
   curl --location --request GET 'http://localhost:8080/drone-api/v1/drones?state=LOADED'
   ```

5. We can always check any attribute of a drone like the current battery level by:
   
   ```bash
   curl --location --request GET 'http://localhost:8080/drone-api/v1/drones/{droneId}'
   ```
   
Enjoy it!
